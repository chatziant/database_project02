from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
import datetime

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'NoSQL_311CI'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/NoSQL_311CI'

mongo = PyMongo(app)

@app.route('/query1', methods=['GET'])
def query01():
    """query01: Find the total requests per type that were created within a specified time range and sort them in a descending order"""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=2:
        return "Error: Needs exactly 2 arguments: start_date, end_date"
    else:
        d1=datetime.datetime.strptime(request.args['start_date'],'%Y-%m-%dT%H:%M:%S')
        d2=datetime.datetime.strptime(request.args['end_date'],'%Y-%m-%dT%H:%M:%S')

        pipe=[
            {"$match": {"creation date":{"$gte":d1,"$lte":d2}}},
            {"$group": {"_id":"$type of service request","total":{"$sum":1}}},
            {"$sort": {"total":-1}}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"service":r["_id"], "total requests":r["total"]})

        return jsonify({"result": output})

@app.route('/query2', methods=['GET'])
def query02():
    """Find the number of total requests per day for a specific request type and time range."""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=3:
        return "Error: Needs exactly 3 arguments: service, start_date, end_date"
    else:
        d1=datetime.datetime.strptime(request.args['start_date'],'%Y-%m-%dT%H:%M:%S')
        d2=datetime.datetime.strptime(request.args['end_date'],'%Y-%m-%dT%H:%M:%S')
        service=request.args['service']

        pipe=[
            {"$match": {
                "type of service request":service,
                "creation date":{"$gte":d1,"$lte":d2}
            }},
            {"$group": {
                "_id":"$creation date",
                "total":{"$sum":1}
            }},
            {"$sort": {"_id":1}}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"day":r["_id"], "total requests":r["total"]})

        return jsonify({"result": output})

@app.route('/query3', methods=['GET'])
def query03():
    """Find the three most common service requests per zipcode for a specific day"""


    col = mongo.db.incidents

    output = []
    if len(request.args)!=1:
        return "Error: Needs exactly 1 argument: date"
    else:
        d=datetime.datetime.strptime(request.args['date'],'%Y-%m-%dT%H:%M:%S')

        pipe=[
            {"$match": {"creation date":d}},
            {"$group": {
                "_id":{
                    "zip":"$zip code",
                    "type":"$type of service request"
                },
                "total":{"$sum":1}
            }},
            {"$sort": {"_id.zip":1,"total":-1}},
            {"$group": {
                "_id":"$_id.zip",
                "types":{
                    "$push":{
                        "type":"$_id.type",
                        "freq":"$total"
                    }
                }
            }},
            {"$project":{
                "_id":1,
                "types":{"$slice":["$types",3]}
            }}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"zip":r["_id"], "services":r["types"]})

        return jsonify({"result": output})

@app.route('/query4', methods=['GET'])
def query04():
    """Find the three least common wards with regards to a given service request type"""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=1:
        return "Error: Needs exactly 1 argument: service"
    else:
        service = request.args['service']

        pipe=[
            {"$match": {"type of service request":service}},
            {"$group":{
                "_id":"$ward",
                "total":{"$sum":1}
            }},
            {"$sort":{"total":1}},
            {"$limit":3}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"ward":r["_id"], "total requests":r["total"]})

        return jsonify({"result": output})

@app.route('/query5', methods=['GET'])
def query05():
    """Find the average completion time per service request for a specific date range"""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=2:
        return "Error: Needs exactly 2 arguments: start_date, end_date"
    else:
        d1=datetime.datetime.strptime(request.args['start_date'],'%Y-%m-%dT%H:%M:%S')
        d2=datetime.datetime.strptime(request.args['end_date'],'%Y-%m-%dT%H:%M:%S')

        pipe=[
            {"$match": {"creation date":{"$gte":d1,"$lte":d2}}},
            {"$project":{
                "_id":0,
                "type of service request":1,
                "diff":{"$subtract":["$completion date", "$creation date"]}
            }},
            {"$group":{
                "_id":"$type of service request",
                "date_diff":{"$avg":"$diff"} #in milliseconds
            }}
        ]

        result=col.aggregate(pipe)

        for r in result:
            #conversion to readable format. not sure if correct...
            secs=int(r['date_diff']/(1000)%60)
            mins=int(r['date_diff']/(1000*60)%60)
            hours=int(r['date_diff']/(1000*60*60)%24)
            days=int(r['date_diff']/(1000*60*60*24))

            difference = str(days)+' days, ' + str(hours) + ' hours,' + str(mins) + ' mins,' + str(secs) + ' secs'
            output.append({"service":r["_id"], "difference":difference})

        return jsonify({"result": output})

@app.route('/query6', methods=['GET'])
def query06():
    """Find the most common service request in a specified bounding box for a specific day
    bl_lat: latitude from bottom left corner of box,
    bl_lon: latitude from bottom left corner of box,
    ur_lat: latitude from upper right corner of box,
    ur_lon: latitude from upper right corner of box"""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=5:
        return "Error: Needs exactly 5 arguments: bl_lat, bl_lon, ur_lat, ur_lon, date"
    else:
        bl_lat=float(request.args['bl_lat'])
        bl_lon=float(request.args['bl_lon'])
        ur_lat=float(request.args['ur_lat'])
        ur_lon=float(request.args['ur_lon'])
        d=datetime.datetime.strptime(request.args['date'],'%Y-%m-%dT%H:%M:%S')

        pipe=[
            {"$match": {
                "creation date":d,
                "location1":{
                    "$geoWithin":{
                        "$box":  [ [bl_lat, bl_lon], [ur_lat, ur_lon] ]
                    }
                }
            }},
            {"$group": {
                "_id":"$type of service request",
                "total":{"$sum":1}
            }},
            {"$sort":{"total":-1}},
            {"$limit":1}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"service":r["_id"], "total requests":r["total"]})

        return jsonify({"result": output})

@app.route('/query7', methods=['GET'])
def query07():
    """Find the fifty most upvoted service requests for a specific day"""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=1:
        return "Error: Needs exactly 1 argument: date"
    else:
        d=datetime.datetime.strptime(request.args['date'],'%Y-%m-%dT%H:%M:%S')

        pipe=[
            {"$match":{"creation date":d}},
            {"$project":{
                "_id":1,
                "upvotes":1
            }},
            {"$unwind":"$upvotes"},
            {"$group":{
                "_id": "$_id",
                "votes":{"$sum":1}
            }},
            {"$sort":{"votes":-1}},
            {"$limit":50}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"service request ID":str(r["_id"]), "total votes":r["votes"]})

        return jsonify({"result": output})

@app.route('/query8', methods=['GET'])
def query08():
    """Find the fifty most active citizens, with regard to the total number of upvotes"""

    col = mongo.db.incidents

    output = []

    if len(request.args)>0:
        return "Error: Doesn't needs any arguments"
    else:
        pipe=[
            {"$unwind":"$upvotes"},
            {"$project":{
                "_id":1,
                "upvotes.name":1,
                "upvotes.address":1,
                "upvotes.telephone":1
            }},
            {"$group":{
                "_id":{
                    "name":"$upvotes.name",
                    "address":"$upvotes.address",
                    "phone":"$upvotes.telephone"
                },
                "total":{"$sum":1}
            }},
            {"$sort":{"total":-1}},
            {"$limit":50}
        ]

        result=col.aggregate(pipe)

        for r in result:
            output.append({"citizen":r["_id"], "total votes":r["total"]})

        return jsonify({"result": output})

@app.route('/query9', methods=['GET'])
def query09():
    """Find the top fifty citizens, with regard to the total number of wards for which they have upvoted an incident"""

    col = mongo.db.incidents

    output = []

    if len(request.args)>0:
        return "Error: Doesn't needs any arguments"
    else:
        pipe=[
            {"$unwind":"$upvotes"},
            {"$project":{
                "upvotes.name":1,
                "upvotes.address":1,
                "upvotes.telephone":1,
                "ward":1
            }},
            {"$group":{
                "_id":{
                    "name":"$upvotes.name",
                    "address":"$upvotes.address",
                    "phone":"$upvotes.telephone"
                },
                "wards":{
                    "$addToSet":"$ward"
                }
            }},
            {"$project":{
                "_id":1,
                "unique_wards":{"$size":"$wards"}
            }},
            {"$sort":{"unique_wards":-1}},
            {"$limit":50}
        ]

        result=col.aggregate(pipe, allowDiskUse=True)

        for r in result:
            output.append({"citizen":r["_id"], "unique_wards":r["unique_wards"]})

        return jsonify({"result": output})

@app.route('/query10', methods=['GET'])
def query10():
    """Find all incident ids for which the same telephone number has been used for more than one names"""

    col = mongo.db.incidents

    output = []

    if len(request.args)>0:
        return "Error: Doesn't needs any arguments"
    else:
        pipe=[
            {"$unwind":"$upvotes"},
            {"$project":{
                "_id":1,
                "upvotes.telephone":1,
                "upvotes.name":1
            }},
            {"$group":{
                "_id": "$upvotes.telephone",
                "names":{
                    "$addToSet":"$upvotes.name"
                },
                "ids":{
                    "$addToSet":"$_id"
                }
            }},
            {"$project":{
                "Incident_IDs":{
                    "$cond":{
                        "if":{ "$gte":[{"$size":"$names"}, 2]}, "then": "$ids","else": None
                    }
                },
            }},
            {"$group":{
                "_id":"$Incident_IDs"
            }}
        ]

        result=col.aggregate(pipe, allowDiskUse=True)

        for r in result:
            output.append({"service ids":str(r["_id"])})

        return jsonify({"result": output})

@app.route('/query11', methods=['GET'])
def query11():
    """Find all the wards in which a given name has casted a vote for an incident taking place in it"""

    col = mongo.db.incidents

    output = []

    if len(request.args)!=1:
        return "Error: Needs exactly 1 argument: name"
    else:

        name = request.args['name']

        pipe=[
            {"$match":{
                "upvotes.name":name
            }},
            {"$project":{
                "_id":0,
                "ward":1
            }},
            {"$group":{
                "_id":"$ward"
            }}
        ]

        result=col.aggregate(pipe, allowDiskUse=True)

        for r in result:
            output.append({"ward":r["_id"]})

        return jsonify({"result": output})

@app.route('/new', methods=['POST'])
def insert_new_incident():
    """Creation of new requests"""

    col = mongo.db.incidents
    output=[]

    data={"create date":datetime.datetime.utcnow(), "status": "Open"}

    #inserts new incident only if the type and fields are supported
    if ('type' not in request.json):
        #type is not included on request
        return "Error: You must at least include a service type!"
    elif(('Tree Debris' not in request.json['type']) and
         ('Garbage Cart Black Maintenance/Replacement' not in request.json['type']) and
         ('Tree Trim' not in request.json['type']) and
         ('Rodent Baiting/Rat Complaint' not in request.json['type']) and
         ('Graffiti Removal' not in request.json['type']) and
         ('Abandoned Vehicle Complaint' not in request.json['type']) and
         ('Pot Hole in Street' not in request.json['type']) and
         ('Pothole in Street' not in request.json['type']) and
         ('Sanitation Code Violation' not in request.json['type']) and
         ('Alley Light Out' not in request.json['type']) and
         ('Street Lights - All/Out' not in request.json['type']) and
         ('Street Light Out' not in request.json['type']) and
         ('Street Light - 1/Out' not in request.json['type'])):
        #type is invalid
        return "Error: Type must be one of the following:Tree Debris, Garbage Cart Black Maintenance/Replacement, Tree Trim, Rodent Baiting/Rat Complaint, Graffiti Removal, Abandoned Vehicle Complaint, Pot Hole in Street, Pothole in Street, Sanitation Code Violation, Alley Light Out, Street Lights - All/Out, Street Light Out or Street Light - 1/Out"
    else:
        type = request.json['type']
        data.update({"type of service request":type})
        #checks if fields are appropriate for each type
        if('Tree Debris' in type):
            if('debris location' in request.json):
                tree_loc = request.json['debris location']
                data.update({"if yes, where is the debris located?": tree_loc})
            if('current activity' in request.json):
                curr_act = request.json['current activity']
                data.update({"current activity": curr_act})
            if('most recent action' in request.json):
                rec_act = request.json['most recent action']
                data.update({"most recent action": rec_act})
        elif('Garbage Cart Black Maintenance/Replacement' in type):
            if('ssa' in request.json):
                ssa = request.json['ssa']
                data.update({"ssa": ssa})
            if('#carts' in request.json):
                carts = request.json['#carts']
                data.update({"number of black carts delivered": carts})
            if('current activity' in request.json):
                curr_act = request.json['current activity']
                data.update({"current activity": curr_act})
            if('most recent action' in request.json):
                rec_act = request.json['most recent action']
                data.update({"most recent action": rec_act})
        elif('Tree Trim' in type):
            if('tree location' in request.json):
                loc_tree = request.json['tree location']
                data.update({"location of trees": loc_tree})
        elif('Rodent Baiting/Rat Complaint' in type):
            if('premises baited' in request.json):
                bait = request.json['premises baited']
                data.update({"number of premises baited": bait})
            if('premises with garbage' in request.json):
                garbage = request.json['premises with garbage']
                data.update({"number of premises with garbage": garbage})
            if('premises with rats' in request.json):
                rats = request.json['premises with rats']
                data.update({"number of premises with rats": rats})
            if('current activity' in request.json):
                curr_act = request.json['current activity']
                data.update({"current activity": curr_act})
            if('most recent action' in request.json):
                rec_act = request.json['most recent action']
                data.update({"most recent action": rec_act})
        elif('Graffiti Removal' in type):
            if('ssa' in request.json):
                ssa = request.json['ssa']
                data.update({"ssa": ssa})
            if('graffiti surface' in request.json):
                surface = request.json['graffiti surface']
                data.update({"what type of surface is the graffiti on?": surface})
            if('graffiti location?' in request.json):
                graf_loc = request.json['where is the graffiti located?']
                data.update({"where is the graffiti located?": graf_loc})
        elif('Abandoned Vehicle Complaint' in type):
            if('ssa' in request.json):
                ssa = request.json['ssa']
                data.update({"ssa": ssa})
            if('model' in request.json):
                model = request.json['model']
                data.update({"vehicle make/model": model})
            if('color' in request.json):
                color = request.json['color']
                data.update({"vehicle color": color})
            if('current activity' in request.json):
                curr_act = request.json['current activity']
                data.update({"current activity": curr_act})
            if('most recent action' in request.json):
                rec_act = request.json['most recent action']
                data.update({"most recent action": rec_act})
            if('days parked' in request.json):
                days = request.json['days parked']
                data.update({"how many days has the vehicle been reported as parked?": days})
        elif('Pot Hole in Street' in type or 'Pothole in Street' in type):
            if('ssa' in request.json):
                ssa = request.json['ssa']
                data.update({"ssa": ssa})
            if('current activity' in request.json):
                curr_act = request.json['current activity']
                data.update({"current activity": curr_act})
            if('most recent action' in request.json):
                rec_act = request.json['most recent action']
                data.update({"most recent action": rec_act})
            if('#potholes' in request.json):
                holes = request.json['#potholes']
                data.update({"number of potholes filled on block": holes})
        elif('Sanitation Code Violation' in type):
            if('violation nature' in request.json):
                violation = request.json['violation nature']
                data.update({"what is the nature of this code violation?": violation})
        # else:
        if ('address' in request.json):
            address = request.json['address']
            data.update({"street address": address})
        if ('zip' in request.json):
            zip = request.json['zip']
            data.update({"zip code" : zip})
        if ('ward' in request.json):
            ward = request.json['ward']
            data.update({"ward" : ward})
        if ('district' in request.json):
            district = request.json['district']
            data.update({"police district" : district})
        if ('area' in request.json):
            area = request.json['area']
            data.update({"community area" : area})
        if ('lat' in request.json) and ('lon' in request.json):
            lat = request.json['lat']
            lon = request.json['lon']
            data.update({"location1" : [lat, lon]})

        new_id = col.insert_one(data).inserted_id

        r=col.find_one({"_id": new_id})

        #return new incident in JSON format for confirmation
        for i in r:
            if "_id" in i:
                output.append({i:str(r[i])})
            else:
                output.append({i:r[i]})

        return jsonify({"result": output})

@app.route('/upvote', methods=['POST'])
def cast_vote():
    """Upvote for citizens by inserting an incindent id and their personal data"""

    col = mongo.db.incidents

    output=[]
    if (("id" not in request.json) or ("name" not in request.json) or ("address" not in request.json) or ("phone" not in request.json)):
        #if not all the necessary fields are inserted on request then exit
        return "Error: Please provide all fields: id, name, address, phone"
    else:
        id = request.json['id']
        name = request.json['name']
        address = request.json['address']
        phone = request.json['phone']

        search=col.find_one({"_id":ObjectId(id)})

        #check if upvotes field already exists
        if 'upvotes' in search:
            s=search['upvotes']

            if({"name":name, "address":address, "telephone":phone} in s):
                #check if the same user is casting vote and exit if it does
                return "Error: The same user cannot vote more than once for the same incident"
                exit(1)
            else:
                #votes already exist
                votes=search['upvotes']
        else:
            #no votes exist
            votes=[]

        #cast the vote
        votes.append({"name": name, "telephone":phone, "address":address})
        search.update({"upvotes":votes})

        col.update({"_id":ObjectId(id)}, search, True)

        r = col.find_one({"_id":ObjectId(id)})

        for i in r:
            if "_id" in i:
                output.append({i:str(r[i])})
            else:
                output.append({i:r[i]})

        return jsonify({"result": output})

if __name__ == '__main__':
    app.run(debug=True)
