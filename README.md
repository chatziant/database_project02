## Database Managment Systems 2018-19
### Project 02

This is a NoSQL project using MongoDB and a RESTful API based on Flask written on Python for the  
postgraduate 'Databases Management Systems' course of the department of Informatics
and Telecommunications from the National and Kapodestrian University of Athens.

In order to run the API the following steps are required:
1. Download the csv files from [here](https://www.kaggle.com/chicago/chicago-311-service-requests)
2. Supposedly you have MongoDB installed on your machine, start up the service:
  ```
  sudo service mongod start
  ```
3. Create a project directory, create a virtual environment, download Flask_PyMongo.
   Make sure csv files are there too.
    ```
   mkdir project
   cd project
   virtualenv -p python3 env
   source env/bin/activate
   pip install Flask-PyMongo Faker
   ```
4. Clone the project:
   ```
   git clone https://gitlab.com/chatziant/database_project02
   ```
5. Make sure .csv file folder is in directory `database_project02`. Fill the database with data from the .csv files:
    ```
    cd database_project02
    cd create
    python db_create.py
    ```
    This will take awhile...
6. Initiate flask webserver:
    ```
    cd ../REST_API
    python db_queries.py
    ```
7. Test the API.

**QUERIES** (feel free to change the parameters)

* https://127.0.0.1:5000/query1?start_date=2016-01-01T00:00:00&end_date=2017-01-01T00:00:00

* https://127.0.0.1:5000/query2?service=Graffiti%20Removal&start_date=2016-01-01T00:00:00&end_date=2016-01-31T00:00:00

* https://127.0.0.1:5000/query3?date=2017-03-15T00:00:00

* https://127.0.0.1:5000/query4?service=Tree%20Debris

* https://127.0.0.1:5000/query5?start_date=2016-01-01T00:00:00&end_date=2017-01-01T00:00:00

* https://127.0.0.1:5000/query6?bl_lat=41.641662537751896&bl_lon=-87.81764092116586&ur_lat=41.861662437751896&ur_lon=-87.61865092116586&date=2016-06-21T00:00:00

* https://127.0.0.1:5000/query7?date=2015-08-30T00:00:00

* https://127.0.0.1:5000/query8

* https://127.0.0.1:5000/query9

* https://127.0.0.1:5000/query10

* https://127.0.0.1:5000/query11?name=John%20Smith

**UPDATES**

* Prepare a JSON string with a new incident

  for example:
    ```
    {
        "type": "Abandoned Vehicle Complaint",
        "address": "Testing 51 Av.",
        "color": "red",
        "zip": 9654782,
        "ward": 193,
        "district": 6,
        "current activity": "blah",
        "lat": 41.93128774110961,
        "lon": -87.66308748385111
    }
    ```
    Upload with:
    https://127.0.0.1:5000/new

* Prepare a JSON string with a new vote:

  for example:
    ```
    {
        "id": "5c41adf73048a90f334f5e0c",
        "name": "John Doe",
        "address": "Foo 99 Street",
        "phone": 0001245789
    }
    ```

    (NOTE: replace id with a real one from your database)

    Upload with:
    https://127.0.0.1:5000/upvote
