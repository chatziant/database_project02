from faker import Faker

def create_citizens(population):
"""Created random citizen data using faker"""
    fake=Faker()
    citizens=[]
    citizen={}
    #votes is a helping attribute that counts citizen's votes for the initialization of database and not used any further
    for i in range(population):
        citizen={"name": fake.name(), "telephone":fake.random_number(10), "address": fake.address(), "votes":0}
        citizens.append(citizen)
        citizen={}

    return citizens
