import pymongo
from pymongo import MongoClient
import sys
import csv
import datetime
from pprint import pprint
import random
from create_citizens import create_citizens
from db_mongo_init import init

def csv_import(colection, path, citizens):
    """Imports all data from the given csv file (path) to the collection of the mongodb database. Data are converted from string to the appropriate datatype according to the nature of the csv data"""
    for p in path:
        print('inserting from:', p)
        with open(p) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',')
            names = reader.fieldnames

            for n in range(len(names)):
                names[n]=names[n].lower()

            total=[]    #list with all dictionaries
            JSON_str={} #dictionary representing JSON format
            for row in reader:
                for i in names:
                    #date convertion
                    if ('creation date' in i) or ('completion date' in i):
                        #check if empty
                        if(row[i]!=''):
                            #convert to datetime object
                            indate=datetime.datetime.strptime(row[i], '%Y-%m-%dT%H:%M:%S')
                            JSON_str.update({i:indate})
                    #integer convertion
                    elif ('zip code' in i) or ('ward' in i) or ('police district' in i) or ('community area' in i) or ('ssa' in i) or ('how many days has the vehicle been reported as parked?' in i) or ('number of black carts delivered' in i) or ('number of premises baited' in i) or ('number of premises with garbage' in i) or ('number of premises with rats' in i):
                        #check if empty
                        if(row[i]!=''):
                            #some fields had floats where integers should be.
                            #in those cases, the floats are converted to integers, changing the original data, but saving memory space to store only integers and not floats for all the fields
                            JSON_str.update({i:int(float(row[i]))})
                    #latitude for geoJSON coordinates
                    elif ('latitude' in i):
                        #check if empty
                        if(row[i]!=''):
                            lat=float(row[i])
                    #longitude for geoJSON coordinates
                    elif ('longitude' in i):
                        #check if empty
                        if(row[i]!=''):
                            lon=float(row[i])
                    #x for geoJSON coordinates (not sure if needed)
                    elif ('x coordinate' in i):
                        #check if empty
                        if(row[i]!=''):
                            x=float(row[i])
                    #y for geoJSON coordinates (not sure if needed)
                    elif ('y coordinate' in i):
                        #check if empty
                        if(row[i]!=''):
                            y=float(row[i])
                    else:
                        JSON_str.update({i:row[i]})
                #geoJSON coordinates for location
                JSON_str.update({"location1":[lat,lon]})
                JSON_str.update({"location2":[x,y]})

                #1/3 of documents get upvotes
                if (random.randint(1,3))==3:
                    #arbitrarilly choose up to 15 upvotes per selected incident
                    votes=random.randint(1,15)
                    #get unique users to fill the upvotes
                    users=random.sample(citizens, votes)
                    #create an upvotes list
                    ups=[]
                    for u in users:
                        #number of upvotes cannot be more than 1000 per document initially
                        if u['votes']<1000:
                            u['votes']=u['votes']+1
                        else:
                            while (u['votes']>=1000):
                                u=random.sample(citizens,1)
                        ups.append(u)
                    #add upvotes list to document
                    JSON_str.update({"upvotes":ups})

                total.append(JSON_str)
                JSON_str={}

        #insert all JSON documents to database
        res=colection.insert_many(total)

    print('Done')

if __name__ == '__main__':
    path=['../chicago-311-service-requests/311-service-requests-abandoned-vehicles.csv', '../chicago-311-service-requests/311-service-requests-alley-lights-out.csv', '../chicago-311-service-requests/311-service-requests-garbage-carts.csv', '../chicago-311-service-requests/311-service-requests-graffiti-removal.csv', '../chicago-311-service-requests/311-service-requests-pot-holes-reported.csv', '../chicago-311-service-requests/311-service-requests-rodent-baiting.csv', '../chicago-311-service-requests/311-service-requests-sanitation-code-complaints.csv', '../chicago-311-service-requests/311-service-requests-street-lights-all-out.csv', '../chicago-311-service-requests/311-service-requests-street-lights-one-out.csv', '../chicago-311-service-requests/311-service-requests-tree-debris.csv', '../chicago-311-service-requests/311-service-requests-tree-trims.csv']

    population= 10000
    [client, db, col] = init()

    print("create random citizens")
    citizens=create_citizens(population)
    print("Done")

    csv_import(col, path, citizens)

    #add some indexes
    print('inserting indexes')
    col.create_index("creation date")
    col.create_index("completion date")
    col.create_index("type of service request")
    col.create_index("location1")
    col.create_index("zip code")
    col.create_index("ward")
    col.create_index("upvotes")
    col.create_index("upvotes.name")
    col.create_index("upvotes.address")
    col.create_index("upvotes.telephone")
    print('Done')
