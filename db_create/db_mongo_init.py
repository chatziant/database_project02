from pymongo import MongoClient

def init():
    client = MongoClient('mongodb://localhost:27017/')
    db = client["NoSQL_311CI"]
    col = db["incidents"]

    return [client, db, col]
